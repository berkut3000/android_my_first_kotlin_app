package com.example.android_my_first_kotlin_app

class Programmer(val name:String,
                 var age:Int,
                 val languages:Array<Language>,
                 val friends: Array<Programmer>? = null)
{
    enum class Language
    {
        KOTLIN,
        SWIFT,
        JAVA,
        C,
        JAVASCRIPT,
        Cpp
    }
    fun code(){
        for(language in languages)
        {
            println("Estoy programando en $language")
        }
    }

}