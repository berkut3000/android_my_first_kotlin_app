package com.example.android_my_first_kotlin_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Leccion 1
        //variableYconstantes()

        //Leccion 2
        //tiposDeDatos()

        //Leccion 3
        //sentencia_if()

        //Leccion 4
        //sentencia_when()

        //Leccion 5
        // arreglos()

        // Leccion 6
        //maps()

        // Leccion 7
        //loops()

        //Leccion8
        //nullSafety()

        //Leccion 9
        Funciones()

        //Leccion 10
        Clases()

    }
    /*
    Aquí vamos a hablar de variables y constantes
     */
    private fun variableYconstantes()
    {
        // Variables
        var my_variable = "hola hackermen"
        var my_first_number = 1

        println(my_variable)

        my_variable = "Bienvenidos a MoureDev"
        println(my_variable)

        //No opdemos asignar un entero a un string
        //my_variable = 1

        var mySecondVariable = "Suscribete"

        println(mySecondVariable)

        mySecondVariable = my_variable

        println(mySecondVariable)

        // Constantes

        val myFirstConstant = "¿Te ha gustado el tutorial?"

        println(myFirstConstant)

        // Una constante no se puede modificar su valor
        //myFirstConstant = "Si te ha gustado, dale like"

        val mySecondConstant : String = my_variable

        println(mySecondConstant)
    }

    //Aquí vamos a hablar de tipos de datos
    private fun tiposDeDatos(){

        //String

        val myString1:String = "Hola Perros"
        val myString2 = "Coman croquetas"
        val myString3:String = myString1 + " " + myString2

        println(myString3)

        //Enteros
        val myInt1:Int = 1
        val myInt2 = 2
        val myInt3 = myInt1 + myInt2
        println(myInt3)

        //Decimales (Float,Double)
        val myFloat:Float = 1.5f
        val myDouble = 1.5
        val myDouble2 = 2.6
        val myDouble3 = 1
        val myDouble4:Double = myDouble + myDouble2 + myDouble3
        println(myDouble4)

        //Boolean
        val myBool = true
        val myBool2 = false

        println( myBool == myBool2)
        println ( myBool && myBool2)
    }

    /*
    Aqui vamos a hablar de la sentencia if
     */
    private fun sentencia_if(){
        val myNumber = 0
        val statusCAN = "RUN"
        val sbat = true
        /*
         Operadores condicionales
         >
         <
         >=
         <=
         ==
         !=
         */
        if(10 == myNumber) {
            println("$myNumber es igual que 10")
        }
        else if (10 <= myNumber)
        {
            println("$myNumber es mayor o igual que 10")
        }

        if(((1 == myNumber) && ("RUN" == statusCAN)) ||(true == sbat))
        {
            println("True Init")
        }
    }

    private fun sentencia_when()
    {
        val country = "España"

        // when con Strings
        when(country){
            "Mexico","España" -> println("El idioma es español")
            //"España" -> println("El idioma es español")
            "China"  -> println("El idioma es mandarin")
            "USA"    -> println("El idiona es ingles")
            else -> println("Pinche Babel")
        }

        // When con ints

        val edad = 10
        when(edad){
            0,1,2 -> println ("Eres un feto")
            in 3..10 -> println ("Eres un chamaque")
            in 11..17 -> println ("Eres un pajero")
            in 18..69 -> println ("Ok, boomer")
            in 70..99 -> println("Adios, vejete")

        }
    }

    fun arreglos(){
        val name = "Brais"
        val surname = "moure"
        val company = "MoureDev"
        val age = "32"

        //Creacion de array
        val my_array: ArrayList<String> = arrayListOf<String>()

        //añadir datos de uno en uno
        my_array.add(name)
        my_array.add(surname)
        my_array.add(company)
        my_array.add(age)
        my_array.add(age)
        my_array.add(age)
        my_array.add(age)

        println(my_array)

        // añadir un conjunto de datos
        my_array.addAll(listOf("Hola", "Bienvenidos"))

        println(my_array)

        //Acceso a datos
        val my_company = my_array[2]

        //Modificacion de datos
        my_array[5] = "Suscribete"

        // Eliminar datos

        my_array.removeAt(4)

        println(my_array)

        // Recorrer datos
        my_array.forEach{
            println(it)
        }

        // Otras funciones
        println(my_array.count())

        my_array.clear()

        println(my_array.count())

    }

    // Aquí, vamos a hablar de mapas, también llamados diccionarios
    fun maps()
    {
        // Sintaxis
        var myMap: Map<String, Int > = mapOf()
        println(myMap)

        //mapa inmutable
        //myMap = mapOf("Bryce" to 1, "Antonio" to 2, "Sara" to 3)

        //mapa mutable
        myMap = mutableMapOf("Bryce" to 1, "Antonio" to 2, "Sara" to 3)
        println(myMap)

        // Añadir un solo valor
        myMap["Ana"] = 7
        myMap.put("maria",8)

        println(myMap)

        myMap.put("Bryce", 3)
        println(myMap)

        myMap.put("Marcos", 3)
        println(myMap)

        // Acceso a datos
        println(myMap["Bryce"])
        println(myMap["Antonio"])

        //Eliminar un dato
        myMap.remove("Bryce")
        println(myMap)
        println(myMap["Bryce"])
    }


    /*
    Aquí vamos a hablar de loops, tambien llamados bucles
     */
    fun loops ()
    {
        //BUcles
        val my_array = listOf("Hola", "Perros", "ya comieron")
        val my_map = mutableMapOf("Bryce" to 1, "Antonio" to 2, "Sara" to 3)

        // For
        for(myString:String in my_array)
        {
            println(myString)
        }

        for(myElement in my_map )
        {
            //println(myElement)
            println("${myElement.key} - ${myElement.value}")
        }

        for(x in 0..10)
        {
            println(x)
        }

        println("Until")
        for(x in 0 until 10)
        {
            println(x)
        }

        println("Step")
        for(x in 0 .. 10 step 2)
        {
            println(x)
        }

        println("Step down")
        for(x in 10 downTo 0 step 2)
        {
            println(x)
        }

        println("Step down")
        for(x in 10 downTo 0 step 3)
        {
            println(x)
        }

        // Rango Numerico
        val myNumericArray = (0 .. 20)
        for(myNum in myNumericArray)
        {
            println(myNum)
        }

        //While

        var x = 0

        while(x < 10)
        {
            println(x)
            x++
        }
    }

    /*
    Aqui vamos a hablar de seguridad contra nulos
     */
    fun nullSafety()
    {
        var myString = "Hola"
        /* Esto daria un error de compilacion */
        //myString = null

        println(myString)

        //Variable null safety
        var mySafetyString: String? = "null safety"
        mySafetyString = null
        println(mySafetyString)

        mySafetyString = "Hola gatos"
        //println(mySafetyString)

        // Error de compilacion
        //mySafetyString = null

        /*
        if (mySafetyString != null ) {
            println(mySafetyString!!)
        } else {
            println(mySafetyString)
        }
        */

        // safeCall

        println(mySafetyString?.length)

        mySafetyString?.let{
            println(it)
        } ?: run {
            println(mySafetyString)
        }
    }

    /*
    Aquí vamos a hablar de funciones
     */
    fun Funciones()
    {
        say_hello()
        say_hello()
        say_hello()

        sayMyName("Antonio")
        sayMyName("Sara")
        sayMyNameAndAge("Antonio", 28)

        var x:Int = sum2numeros(3,5)
        println(x)
        println(sum2numeros(3,4))
        println(sum2numeros(3,sum2numeros(8,8)))


    }

    // Funcion Simple
    fun say_hello()
    {
        println("Hola")
    }

    // Funciones con un parametro de entrada
    fun sayMyName(miNombre:String)
    {
        println("Mi nombre es $miNombre")
    }

    fun sayMyNameAndAge(miNombre:String, miEdad:Int)
    {
        println("Mi nombre es $miNombre y mi edad es $miEdad")
    }

    // Funciones con valor de retorno
    fun sum2numeros(primerNumero: Int, segundoNumero: Int) : Int
    {
        val sum:Int = primerNumero + segundoNumero
        return sum
    }

    /*
    Aqui vamos a hablar de clases
     */
    fun Clases()
    {
        val ant_prog = Programmer("Antonio",28, arrayOf(Programmer.Language.KOTLIN, Programmer.Language.C))
        println(ant_prog.name)

        ant_prog.age = 29
        println(ant_prog.age)
        ant_prog.code()
        //println(ant_prog)

        val sara_prog = Programmer("Sara", 34, arrayOf(Programmer.Language.Cpp, Programmer.Language.SWIFT),arrayOf(ant_prog))
        sara_prog.code()
        println("${sara_prog.friends?.first()?.name} es amigo de ${sara_prog.name}")

    }

}
